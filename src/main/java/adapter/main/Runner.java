package adapter.main;

import java.util.ArrayList;

import adapter.list.ListAdapter;
import adapter.list.impl.ListAdapterImpl;

public class Runner {
	public static void main(String[] args) {
		ListAdapter<String> listAdapter = new ListAdapterImpl<>(new ArrayList<String>());
		listAdapter.push("Hello");
		listAdapter.push("World");
		System.out.println(listAdapter.pop());
	}
}
