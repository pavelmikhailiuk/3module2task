package adapter.list.impl;

import java.util.ArrayList;
import java.util.List;

import adapter.list.ListAdapter;

public class ListAdapterImpl<E> implements ListAdapter<E> {

	private List<E> baseList;

	public ListAdapterImpl(List<E> baseList) {
		if (baseList != null) {
			this.baseList = baseList;
		} else {
			baseList = new ArrayList<>();
		}
	}

	@Override
	public void push(E obj) {
		if (obj != null) {
			baseList.add(obj);
		}
	}

	@Override
	public E pop() {
		return baseList.size() > 0 ? baseList.remove(baseList.size() - 1) : null;
	}
}
