package adapter.list;

public interface ListAdapter<E> {

	void push(E obj);

	E pop();
}
